---
marp: true

theme: default
header: ''
footer: ''
backgroundImage: url('./styles/background.png')
title: Beispiel der Integration eines interaktiven Elements
author: Christian Willberg
---



## Integration

![bg cover](./styles/background.png)

<iframe src="https://perilab-results.nimbus-extern.dlr.de/models/DCB?step=65&variable=Damage&displFactor=400" width="1150" height="600"></iframe>

